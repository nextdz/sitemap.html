<?php

CModule::IncludeModule("iblock");
CModule::IncludeModule("main");


class Int1cBHtmlSiteMap
{

    private $arrParam = [];

    function __construct() {
        
    }

    public function setArrParam($arrParam) {
        $this->arrParam = $arrParam;
    }

    /**
     * 
     * @param type $str
     * @return boolean
     */
    public function intStrToArray($str = '') {
        if (empty($str))
            return false;

        $output = [];

        $sanitize_str = preg_replace('~[^\d||\,]~', '', $str);

        if (preg_match('~\,~', $sanitize_str) > 0) {
            $output = explode(',', $sanitize_str);
        } else {
            $output = $sanitize_str;
        }

        return $output;
    }

    /**
     * Функция возвращает массив данных инфоблока.
     * @param type $iBId
     * @return boolean
     */
    public function getIBlockData($iBId = '') {
        if (empty($iBId))
            return false;

        $res = CIBlock::GetByID($iBId);
        if ($ar_res = $res->GetNext()) {
            return $ar_res;
        }
        return false;
    }

    /**
     * Функция возвращает список каталогов.
     * @param type $iBId
     * @return boolean
     */
    public function getIBlockSectionList($iBId = '', $excludeCatalogs) {
        
        if (empty($iBId))
            return false;

        $arExcludedCatalogs = [-1];
        
        if ($this->arrParam['EXCLUDED_CATALOGS'])
            $arExcludedCatalogs = $this->arrParam['EXCLUDED_CATALOGS'];
        
        $allArr = [];

        $res = CIBlockSection::GetList(Array('left_margin' => 'ASC'), Array('IBLOCK_ID' => $iBId,
                    'ACTIVE' => 'Y',
                    '!ID' => $arExcludedCatalogs,
                    '!SECTION_ID' => $arExcludedCatalogs
                ),
                Array(/* 'ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'CODE', 'SECTION_PAGE_URL', 'LEFT_MARGIN' */),
                false);

        while ($arr = $res->GetNext()) {
            if (!empty($arr["ID"]) && !empty($arr["NAME"])) {
                
                $allArr[$arr["ID"]] = $arr;
            }
        }

        if (count($allArr) > 0) {
            return $allArr;
        }

        return false;
    }

    /**
     * Функция возвращает список элементов.
     * @param type $iBId
     * @return boolean
     */
    public function getIBlockElementList($arOrder = [], $arFilter = [], $arGroupBy = false, $arNavStartParams = false, $arSelectFields = []) {

        if (!$arOrder)
            $arOrder = ['left_margin' => 'ASC'];

        if (!$arFilter)
            return false;

        $arExcludedElements = [];
        $allElements = [];

        if ($this->arrParam['EXCLUDED_ELEMENTS'])
            $arExcludedElements = $this->arrParam['EXCLUDED_ELEMENTS'];

        $getElements = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

        while ($arrElements = $getElements->GetNext()) {
            $arrElement = [];

            $arrElement['ID'] = $arrElements['ID'];
            $arrElement["NAME"] = $arrElements["NAME"];
            $arrElement["URL"] =  $site_root_url . $arrElements['DETAIL_PAGE_URL'];
            $arrElement["IBLOCK_SECTION_ID"] = $arrElements['IBLOCK_SECTION_ID'];
            $arrElement["TIPE"] = 'ELEMENT';

            $allElements[$arrElements['ID']] = $arrElement;
        }
        return $allElements;
    }

    /**
     * Функция формирует масив в иерархическом порядке вложенности каталогов.
     * @param type $iCatObj массив перебираемых элементов
     * @param type $needle искомый id родительского элемента
     * @param type $elObj массив дочернего элемента
     * @return array
     */
    public static function intFindParrentCatalog($iCatObj, $needle = '', $elObj) {
                
        if (!is_array($iCatObj) || !is_array($elObj) || $needle == '')
            return false;
        
        
        if ($iCatObj['ID'] == $needle) {

            $iCatObj['CHILD'][$elObj['ID']] = $elObj;
            
        } elseif (isset($iCatObj['CHILD']) && $iCatObj["TIPE"] == 'CAT') {

            $tempICatCholdObj = $iCatObj['CHILD'];

            foreach ($tempICatCholdObj as $ikey => $ival) {
                
                $tempArrCatResult = self::intFindParrentCatalog($tempICatCholdObj[$ikey], $needle, $elObj);

                if ($tempArrCatResult) {
                    $iCatObj['CHILD'][$ikey] = $tempArrCatResult;
                }
            }
        }
        return $iCatObj;
    }
    
    /**
     * Проходимся по папкам, которые нужно включить в карту сайта.
     * @return string
     */
    public function getFoldersList() {

        if ($arExcludedFolders = $this->arrParam['EXCLUDED_FOLDERS']) {
            foreach ($arExcludedFolders as $folder) {
                if (empty($folder)) {
                    continue;
                }
                $arExcludedFolders[] = trim($folder);
            }
        }
        
        if ($arIncludedFolders = $this->arrParam['INCLUDED_FOLDERS']) {
            foreach ($arIncludedFolders as $folder) {
                if (empty($folder)) {
                    continue;
                }
                $arIncludedFolders[] = trim($folder);
            }
        }

        $directory = $_SERVER["DOCUMENT_ROOT"];
        $root_dir = scandir($directory);
        foreach ($root_dir as $dir) {
            if ($dir == "." || $dir == ".." || in_array($dir, $arExcludedFolders) || !in_array($dir, $arIncludedFolders) || is_file($dir) || strpos($dir, ".") !== false) {
                continue;
            } else {

                foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($_SERVER["DOCUMENT_ROOT"] . "/" . $dir)) as $file) {
                    $full_pathname = $file->getPathname();
                    if (strpos($full_pathname, "index.php") === false) {
                        continue;
                    } else {
                        $url_path = str_replace($_SERVER["DOCUMENT_ROOT"], "", str_replace("index.php", "", $full_pathname));
                        $arFolders[$url_path]["ROOT_PATH"] = str_replace("index.php", "", $full_pathname);
                        $arFolders[$url_path]["PATH"] = str_replace($_SERVER["DOCUMENT_ROOT"], "", str_replace("index.php", "", $full_pathname));
                        if (file_exists(str_replace("index.php", "", $full_pathname) . ".section.php")) {
                            $sSectionName = false;
                            include str_replace("index.php", "", $full_pathname) . ".section.php";
                            if ($sSectionName) {
                                $arFolders[$url_path]["NAME"] = $sSectionName;
                            } elseif ($arDirProperties["TITLE"]) {
                                $arFolders[$url_path]["NAME"] = $arDirProperties["TITLE"];
                            } else {
                                $arFolders[$url_path]["NAME"] = "Без названия";
                            }
                        } else {
                            $arFolders[$url_path]["NAME"] = "Без названия";
                        }
                    }
                }
            }
        }
        return $arFolders;
    }

}
