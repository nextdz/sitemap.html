<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
global $USER;
if (file_exists(__DIR__ . '/Classes/Int1cBHtmlSiteMap.php')) {

//    require_once __DIR__ . '/Classes/Int1cBHtmlSiteMap.php';
}
class IntGetHtmlMapList {

     public function __construct() {
         
     }

     /**
     * Функция формирует готовый список элементов, с учетом вложенности
     * @param type $arResult
     * @return string
     */
     public static function getIBlockList($iBlockArray = false) {
        $output = '';

        if (is_array($iBlockArray)) {

            foreach ($iBlockArray as $key => $val) {
                                
                $has_parent_cat = !empty($val["IBLOCK_SECTION_ID"]) ? $val["IBLOCK_SECTION_ID"] : false;
                $this_type = !empty($val["TIPE"]) ? mb_strtolower($val["TIPE"]) : false;
                $has_child = isset($val["CHILD"]) ? "has_child" : "single";
                $this_url =  $val["URL"];
                $sub_list_marker = $val["TIPE"] != "ELEMENT" ? "sub_list " : '';
                
                if ($has_child == 'has_child') {
                $output .= "<li class='tipe_{$this_type} {$has_child} id_" . $val["ID"] . "'>";
                $output .= "<a href='{$this_url}' >" . $val["NAME"] . "</a>";

                
                    $output .= "<ul class='{$sub_list_marker} type_" . $val["TIPE"] . " id_" . $val["ID"] . "'>";
                        $output .= self::getIBlockList($val["CHILD"]);
                    $output .= "</ul>";
                } elseif($val["TIPE"] == 'ELEMENT') {
                    
                    $output .= "<li class='tipe_" . $val["TIPE"] . " {$has_child}'>";
                        $output .= "<a href='{$this_url}' >" . $val["NAME"] . "</a>";
                    $output .= "</li>";
                } else {    
                    $output .= "<li class='tipe_{$this_type} {$has_child} id_" . $val["ID"] . "'>";
                    $output .= "<a href='{$this_url}' >" . $val["NAME"] . "</a>";
                    $output .= "</li>";
                }
                
                if ($has_child == 'has_child')
                $output .= "</li>";
            }

            return $output;
        }

        return false;
    }

    /**
      * Функция формирует готовый список папок, в виде li элементов.
      * @param type $arResult
      * @return string
      */
     public static function getFoldersList($arResult = false) {
        $output = '';

        if (is_array($arResult)) {

            foreach ($arResult as $key => $val) {
                
                
                if(!empty($val["PATH"]) && !empty($val["NAME"])){
                    
                $url = $site_root_url . $val["PATH"];
                $slug = mb_strtolower(str_replace('/', '', $url));
                $name = $val["NAME"];
                
                    $output .= "<li class='tipe_page id_{$slug}'>";
                    $output .= "<a href='{$url}' >{$name}</a>";
                    $output .= "</li>";
                }
                
            }

            return $output;
        }

        return false;
    }
    
    
    public static function getIBlockGrid($iBlockArray = false) {
        $output = '';

        if (is_array($iBlockArray)) {
                                    
            foreach ($iBlockArray as $key => $val) :
                
            $has_child = isset($val["CHILD"]) ? "has_child" : "single";  
            if ($has_child == 'has_child') {
               $output .= "<tr>"
                            . "<td>".$val["NAME"]."</td>"
                            . "<td>".$val["URL"]."</td>"
                        . "</tr>";

               
                $output .= self::getIBlockGrid($val["CHILD"]);
                
                } elseif($val["TIPE"] == 'ELEMENT') {
                $output .= "<tr>"
                            . "<td>".$val["NAME"]."</td>"
                            . "<td>".$val["URL"]."</td>"
                        . "</tr>";
                } else {    
                $output .= "<tr>"
                            . "<td>".$val["NAME"]."</td>"
                            . "<td>".$val["URL"]."</td>"
                        . "</tr>";
                }


    
            endforeach;

            return $output;
        }

        return false;
    }
    
}

?>

<ul class="sitemap_html">
    <li><a href="/">Главная</a></li>
    <?= IntGetHtmlMapList::getFoldersList($arResult["FOLDERS"]);?>
    <?= IntGetHtmlMapList::getIBlockList($arResult["IBLOCK"]); ?>
    
    <?php
    global $USER;
    if($USER->IsAdmin() && $_SERVER['REMOTE_ADDR'] == '188.163.45.230'){

            echo "<table>"
                    . "<thead><tr>"
                        . "<td>Title</td>"
                        . "<td>Url</td>"
                    . "</tr>"
                    . "</thead>"
                    . "<tbody>";
            
            echo IntGetHtmlMapList::getIBlockGrid($arResult["IBLOCK"]);
            
            echo "</tbody> </table>";
        
    }
    ?>
</ul>


