jQuery(document).ready(function($){
    $('li.has_child').on('click', function(e){
        if($(e.target).hasClass('hide_child')){
            $(e.target).removeClass('hide_child');
            $(e.target).addClass('show_child');
            e.stopPropagation();
        }else{
            $(e.target).addClass('hide_child');
            $(e.target).removeClass('show_child');
            e.stopPropagation();
        }
    });
});