<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

CModule::IncludeModule("iblock");

$arIBlock = array();
$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch()){
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage('SELECT_INFOBLOKS'),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arIBlock,
			"DEFAULT" => '',
		),
		"INCLUDE_ELEMENTS" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage('INCLUDE_ELEMENTS'),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
                "INCLUDED_FOLDERS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage('INCLUDE_FOLDERS'),
			"TYPE" => "STRING",
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
		),
		"EXCLUDED_FOLDERS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage('EXCLUDE_FOLDERS'),
			"TYPE" => "STRING",
			"MULTIPLE" => "Y",
			"DEFAULT" => array(0=>"bitrix",1=>"upload",2=>"search",3=>"cgi-bin",4=>"images",),
		),
            	"EXCLUDED_CATALOGS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage('EXCLUDE_CATALOGS'),
			"TYPE" => "STRING",
		),
            	"EXCLUDED_ELEMENTS" => Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage('EXCLUDE_ELEMENTS'),
			"TYPE" => "STRING",
		),
//		"SET_NAVIGATION" => Array(
//			"PARENT" => "PAGER_SETTINGS",
//			"NAME" => 'Выводить пагинацию',
//			"TYPE" => "CHECKBOX",
//			"DEFAULT" => "N",
//			"REFRESH" => "Y"
//                    ),
//		"ELEMENTS_PER_PAGE" => Array(
//			"PARENT" => "PAGER_SETTINGS",
//			"NAME" => "Каталогов на страницу",
//			"TYPE" => "STRING",
//			"DEFAULT" => intVal(COption::GetOptionString("HTML_sitemap", "ELEMENTS_PER_PAGE", "5"))
//		),
	),
);
?>