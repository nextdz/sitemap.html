<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die();

CModule::IncludeModule("iblock");
CModule::IncludeModule("main");

global $APPLICATION;

$arIblocks = $arParams["IBLOCK_ID"];
$site_https_prot = $_SERVER["HTTPS"] == 'on' ? 'https://' : 'http://' ;
$site_root_url = $site_https_prot . SITE_SERVER_NAME;

//echo dump([$site_root_url, SITE_DIR]);
if (file_exists(__DIR__ . '/Classes/Int1cBHtmlSiteMap.php')) {

    require_once __DIR__ . '/Classes/Int1cBHtmlSiteMap.php';


    $int1cBHtmlSiteMap = new Int1cBHtmlSiteMap();

    //EXCL - ELEMENTS
    $excludeElements = $int1cBHtmlSiteMap->intStrToArray($arParams["EXCLUDED_ELEMENTS"]);

    //EXCL - CATALOGS
    $excludeCatalogs = $int1cBHtmlSiteMap->intStrToArray($arParams["EXCLUDED_CATALOGS"]);

    // Передаем в обект параметры компонента.
    $int1cBHtmlSiteMap->setArrParam(['EXCLUDED_CATALOGS' => $excludeCatalogs,
                                    'EXCLUDED_ELEMENTS' => $excludeElements,
                                    'EXCLUDED_FOLDERS' => $arParams["EXCLUDED_FOLDERS"],
                                    'INCLUDED_FOLDERS'=>$arParams["INCLUDED_FOLDERS"],
                                    ]);
    $arrResult = [];

    /**
     *  Перебираем выбранные инфоблоки
     */
    foreach ($arIblocks as $IblockID) {
         
        $arrCat = [];
        /*
         * Инфо. текущего инфоблока
         */
        $currentIBlockData = $int1cBHtmlSiteMap->getIBlockData($IblockID);
        
        $iblock_url = str_replace( ['#SITE_DIR#', '#IBLOCK_CODE#'],[ $site_root_url.'/', $currentIBlockData["CODE"]], $currentIBlockData["LIST_PAGE_URL"]);

        $arResult['IBLOCK'][$IblockID]["ID"] = $currentIBlockData["ID"];
        $arResult['IBLOCK'][$IblockID]["NAME"] = $currentIBlockData["NAME"];
        $arResult['IBLOCK'][$IblockID]["URL"] = $iblock_url;
        $arResult['IBLOCK'][$IblockID]["IBLOCK_SECTION_ID"] = $currentIBlockData["IBLOCK_SECTION_ID"];
        $arResult['IBLOCK'][$IblockID]["TIPE"] = "IBLOCK";

        /*
         * Рубрики(каталоги) текущего инфоблока
         */
        $arrCat = $int1cBHtmlSiteMap->getIBlockSectionList($IblockID, $excludeCatalogs);
             
        //Если есть рубрики, тогда проходимся по ним, перестраиваем в порядке вложенности и получаем их елементы.
        if ($arrCat) {
            foreach ($arrCat as $key => $currentSectionData) {

                $arrCatTemp = [];
                $tempChild = $int1cBHtmlSiteMap->getIBlockElementList(['left_margin' => 'ASC'], ["IBLOCK_ID" => $IblockID, "SECTION_ID" => $currentSectionData["ID"], "ACTIVE" => "Y", '!ID' => $excludeElements], false, false, ['ID', "NAME", "DETAIL_PAGE_URL", 'IBLOCK_SECTION_ID']);              
                
                $arrCatTemp["ID"] = $currentSectionData["ID"];
                $arrCatTemp["NAME"] = $currentSectionData["NAME"];
                $arrCatTemp["URL"] = $site_root_url. $currentSectionData["SECTION_PAGE_URL"];
                $arrCatTemp["IBLOCK_SECTION_ID"] = $currentSectionData['IBLOCK_SECTION_ID'];
                $arrCatTemp["DEPTH_LEVEL"] = $currentSectionData['DEPTH_LEVEL'];
                $arrCatTemp["TIPE"] = "CAT";
                
                if(!empty($tempChild))
                $arrCatTemp['CHILD'] = $tempChild;
                
                if (empty($currentSectionData['IBLOCK_SECTION_ID'])) { // root node 
                    
                    $arrCat[$currentSectionData["ID"]] = $arrCatTemp;
                    
                } else { // sub node
                    unset($arrCat[$key]);

                    foreach ($arrCat as $ikey => $ival) {
                        
                        $tempArrCatResult = Int1cBHtmlSiteMap::intFindParrentCatalog($ival, $currentSectionData['IBLOCK_SECTION_ID'], $arrCatTemp);

                        if ($tempArrCatResult) {
                            
                            $arrCat[$ikey] = $tempArrCatResult;
                            
                        }
                    }
                }
            }
        $arResult['IBLOCK'][$IblockID]["CHILD"] = $arrCat;
        } else {
        // Если нет каталогов, пробуем получиль елементы инфоблока.
            $arrElements = $int1cBHtmlSiteMap->getIBlockElementList(['left_margin' => 'ASC'], ["IBLOCK_ID" => $IblockID, "ACTIVE" => "Y", '!ID' => $excludeElements], false, false, ['ID', "NAME", "DETAIL_PAGE_URL", 'IBLOCK_SECTION_ID']);

            $arResult['IBLOCK'][$IblockID]["CHILD"] = $arrElements; 
            
        }
        
    }

    $arResult["FOLDERS"] = $int1cBHtmlSiteMap->getFoldersList();
}

$this->IncludeComponentTemplate();
?>