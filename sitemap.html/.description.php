<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('DESCR_NAME'),
	"DESCRIPTION" => GetMessage('DESCR_DESCRIPTION'),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "HTML_sitemap"
	),
);

?>